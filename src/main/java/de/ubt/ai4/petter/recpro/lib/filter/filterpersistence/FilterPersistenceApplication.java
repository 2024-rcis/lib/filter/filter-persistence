package de.ubt.ai4.petter.recpro.lib.filter.filterpersistence;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"de.ubt.ai4.petter.recpro.lib.filter.filterpersistence", "de.ubt.ai4.petter.recpro.lib.bpmpersistence"})
public class FilterPersistenceApplication {

	public static void main(String[] args) {
		SpringApplication.run(FilterPersistenceApplication.class, args);
	}

}

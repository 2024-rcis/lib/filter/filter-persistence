package de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.filter;

import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@AllArgsConstructor
@Getter
@Setter
@Entity

@DiscriminatorValue("PROCESS_AWARE")
public class ProcessAwareFilterInstance extends FilterInstance implements Serializable {

    @Override
    public ProcessAwareFilterInstance copy() {
        ProcessAwareFilterInstance copy = (ProcessAwareFilterInstance) super.copy();
        copy.setId(null);
        return copy;
    }
}

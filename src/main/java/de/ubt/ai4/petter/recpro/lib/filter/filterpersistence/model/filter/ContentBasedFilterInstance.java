package de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.filter;

import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@AllArgsConstructor
@Getter
@Setter
@Entity

@DiscriminatorValue("CONTENT_BASED")
public class ContentBasedFilterInstance extends FilterInstance implements Serializable {
    @Override
    public ContentBasedFilterInstance copy() {
        ContentBasedFilterInstance copy = (ContentBasedFilterInstance) super.copy();
        copy.setId(null);
        return copy;
    }
}

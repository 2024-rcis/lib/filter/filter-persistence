package de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.result;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.springframework.util.SerializationUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

@Entity
public class FilterResult implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany
    @Cascade(CascadeType.ALL)
    private List<FilterResultItem> items = new ArrayList<>();

    public FilterResult copy() {
        FilterResult copy = SerializationUtils.clone(this);
        copy.setId(null);
        copy.setItems(FilterResultItem.copy(this.getItems()));
        return copy;
    }

    public FilterResultItem getResultItemByTaskId(Long taskId) {
        Optional<FilterResultItem> filtered = this.getItems().stream().filter(item -> item.getTaskId().equals(taskId)).findFirst();
        return filtered.orElseGet(FilterResultItem::new);
    }
}

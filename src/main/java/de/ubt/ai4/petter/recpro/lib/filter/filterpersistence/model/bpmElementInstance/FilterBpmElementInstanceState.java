package de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.bpmElementInstance;

public enum FilterBpmElementInstanceState {
    TRUE,
    FALSE,
    NEUTRAL
}

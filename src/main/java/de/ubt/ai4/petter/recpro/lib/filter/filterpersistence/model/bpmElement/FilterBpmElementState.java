package de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.bpmElement;

public enum FilterBpmElementState {
    TRUE,
    FALSE,
    NEUTRAL
}

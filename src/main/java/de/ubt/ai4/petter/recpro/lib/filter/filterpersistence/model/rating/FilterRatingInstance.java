package de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.rating;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.util.SerializationUtils;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class FilterRatingInstance implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String ratingId;

    public FilterRatingInstance copy() {
        FilterRatingInstance copy = SerializationUtils.clone(this);
        copy.setId(null);
        return copy;
    }
}

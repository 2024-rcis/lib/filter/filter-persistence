package de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.result;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.util.SerializationUtils;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

@Entity
public class FilterResultItem implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long taskId;
    private Double value;
    private Integer position;
    private String description;
    private boolean visible;

    public FilterResultItem copy() {
        FilterResultItem copy = SerializationUtils.clone(this);
        copy.setId(null);
        return copy;
    }

    public static List<FilterResultItem> copy(List<FilterResultItem> instances) {
        return instances.stream().map(FilterResultItem::copy).toList();
    }

    public static FilterResultItem findByTaskId(Long taskId, List<FilterResultItem> items) {
        Optional<FilterResultItem> res = items.stream().filter(item -> item.getTaskId().equals(taskId)).findFirst();
        return res.orElse(new FilterResultItem());
    }
}

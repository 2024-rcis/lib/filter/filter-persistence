package de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.service;

import de.ubt.ai4.petter.recpro.lib.filter.basefilter.model.FilterType;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.filter.FilterInstance;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service

@Component("database")
public class FilterInstancePersistenceServiceImpl implements IFilterInstancePersistenceService {

    private FilterInstanceRepository repository;

    @Override
    public FilterInstance save(FilterInstance filterInstance) {
        return repository.saveAndFlush(filterInstance.copy());
    }

    @Override
    public List<FilterInstance> getAll() {
        return repository.findAll();
    }

    @Override
    public Optional<FilterInstance> getById(Long id) {
        return repository.findById(id);
    }

    @Override
    public Optional<FilterInstance> getLatestByAssigneeIdAndFilterType(String assigneeId, FilterType filterType) {
        return repository.findTopByAssigneeIdAndFilterTypeOrderByIdDesc(assigneeId, filterType);
    }

    @Override
    public Optional<FilterInstance> getByTasklistIdAndFilterType(Long tasklistId, FilterType filterType) {
        return repository.findByTasklistIdAndFilterType(tasklistId, filterType);
    }

}

package de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.service;

import de.ubt.ai4.petter.recpro.lib.filter.basefilter.model.FilterType;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.filter.FilterInstance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FilterInstanceRepository extends JpaRepository<FilterInstance, Long> {
    Optional<FilterInstance> findTopByAssigneeIdAndFilterTypeOrderByIdDesc(String assigneeId, FilterType filterType);
    Optional<FilterInstance> findByTasklistId(Long tasklistId);
    Optional<FilterInstance> findByTasklistIdAndFilterType(Long tasklistId, FilterType filterType);

}

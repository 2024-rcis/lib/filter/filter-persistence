package de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.filter;

import de.ubt.ai4.petter.recpro.lib.filter.basefilter.model.TasklistOrder;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

@Entity

@DiscriminatorValue("BASE")
public class BaseFilterInstance extends FilterInstance {
    private boolean ascending;

    @Enumerated(EnumType.STRING)
    private TasklistOrder tasklistOrder;

}

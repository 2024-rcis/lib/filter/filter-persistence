package de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.bpmElement;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.util.SerializationUtils;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

@Entity
public class FilterBpmElementInstance implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String recproElementId;

    @Enumerated(EnumType.STRING)
    private FilterBpmElementState state;

    public FilterBpmElementInstance copy() {
        FilterBpmElementInstance copy = SerializationUtils.clone(this);
        copy.setId(null);
        return copy;
    }

    public static List<FilterBpmElementInstance> copy(List<FilterBpmElementInstance> instances) {
        return instances.stream().map(instance -> instance.copy()).toList();
    }
}
